import axios from 'axios'

export function fetchData(){
    const url = 'https://jsonplaceholder.typicode.com/users'
    axios.get(url)
        .then(res=> {
            return res.data
        }).catch(err => err)

}
