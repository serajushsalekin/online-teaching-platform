import React from "react"
import {fetchData} from '../api/handler'

export default function Data() {
    return(
        <div>
            <button onClick={fetchData}>Fetch</button>
        </div>
    )
}
